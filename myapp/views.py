from django.shortcuts import render
from django.shortcuts import HttpResponse,reverse
from myapp import models

# Create your views here.

from django.shortcuts import HttpResponse,render,redirect

def login(request):
    if request.method == 'GET':
        return render(request,'login.html')

    else:
        u = request.POST.get('user')
        p = request.POST.get('pwd')
        if u == 'root' and p == '123':
            #登录成功
            obj = redirect('/myapp/index')
            obj.set_cookie('ticket','asfdgfewrgslrgesafq4',max_age=10)
            return obj
        else:
            #登录失败
            return render(request,'login.html',{'msg':'用户名或密码错误，请重新登录！'})


def index(request):
    tk = request.COOKIES.get('ticket')
    if not tk:
        return render(request,'login.html')
    else:
        group_list = models.GroupInfo.objects.all()
        return  render(request,'index.html',{'group_list':group_list})


def mydel(request,a1):
    models.GroupInfo.objects.filter(id=a1).delete()
    group_list = models.GroupInfo.objects.all()
    return render(request, 'index.html', {'group_list': group_list})


def edit(request,a2):
    if request.method == 'GET':
        gitem = models.GroupInfo.objects.get(id=a2)
        return  render(request,'edit.html',{'gitem':gitem})
    else:
        title = request.POST.get('titlenew')
        serverip = request.POST.get('ipnew')
        servertype = request.POST.get('typenew')
        models.GroupInfo.objects.filter(id=a2).update(title=title,serverip=serverip,servertype=servertype)
        group_list = models.GroupInfo.objects.all()
        return render(request, 'index.html', {'group_list': group_list})


def create(request):
    if request.method == 'GET':
        return  render(request,'create.html')
    else:
        title = request.POST.get('title')
        serverip = request.POST.get('serverip')
        stype = request.POST.get('stype')
        models.GroupInfo.objects.create(title=title,serverip=serverip,servertype=stype)
        group_list = models.GroupInfo.objects.all()
        return render(request, 'index.html', {'group_list': group_list})

def layout(request):
    return  render(request,'layout.html')