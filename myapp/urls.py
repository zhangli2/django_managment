from django.conf.urls import url
from myapp import views

urlpatterns = [
    #url(r'^admin/', admin.site.urls),
    url(r'^login/$',views.login),
    url(r'^index/$',views.index),
    url(r'^del/(\w+)',views.mydel),
    url(r'^edit/(\w+)', views.edit),
    url(r'^create/$',views.create),



]
